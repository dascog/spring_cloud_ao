package com.example.timeproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.annotation.InboundChannelAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;


@SpringBootApplication
public class TimeProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeProducerApplication.class, args);
    }
}

@EnableBinding(Source.class)
class TimeProducer {
    @InboundChannelAdapter(Source.OUTPUT)
    public String timerMessageSource() {
        return "Hello! It's "+new SimpleDateFormat().format(new Date());
    }
}
