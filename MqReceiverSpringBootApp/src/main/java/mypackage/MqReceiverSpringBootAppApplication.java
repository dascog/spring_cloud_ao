package mypackage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqReceiverSpringBootAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqReceiverSpringBootAppApplication.class, args);
    }

}
