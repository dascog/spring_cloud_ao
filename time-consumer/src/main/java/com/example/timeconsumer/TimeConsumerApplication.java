package com.example.timeconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
public class TimeConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeConsumerApplication.class, args);
    }

}

@EnableBinding(Sink.class)
class TimeConsumer {
    @StreamListener(Sink.INPUT)
    public void log(String message) {
        System.out.println(message);
    }
}
