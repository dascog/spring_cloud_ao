package mypackage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class MqSenderSpringBootAppApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(MqSenderSpringBootAppApplication.class, args);

        MySender sender = context.getBean(MySender.class);

        sender.sendMessage("Huey");
        sender.sendMessage("Louis");
        sender.sendMessage("Dewey");
    }
}
